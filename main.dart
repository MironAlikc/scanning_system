import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:scanning_system/components/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  int _widgetId = 1;
  bool selected = false;

  Widget _renderWidget1() {
    return Container(key: const Key('first'), child: animCircle());
  }

  Widget _renderWidget2() {
    return Container(key: const Key('second'), child: animCircleTwo());
  }

  Widget _renderWidget() {
    return _widgetId == 1 ? _renderWidget1() : _renderWidget2();
  }

  void _updateWidget() {
    setState(() {
      selected = true;
      _widgetId = _widgetId == 1 ? 2 : 1;
    });
  }

  void _updateWidgetToAnim() {
    setState(() {
      selected = false;
      _widgetId = _widgetId == 1 ? 2 : 1;
    });
  }

  late final AnimationController _controller2 = AnimationController(
    duration: const Duration(milliseconds: 1500),
    vsync: this,
  );
  late final Animation<double> _animation2 =
      Tween(begin: 1.0, end: 0.0).animate(_controller2);

  late final AnimationController _controller = AnimationController(
    duration: const Duration(milliseconds: 1500),
    vsync: this,
  );
  late final Animation<double> _animation =
      Tween(begin: 1.0, end: 0.0).animate(_controller);

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  Container adContainer = Container();
  //InterstitialAd? _interstitialAd;
  bool adIsLoaded = false;
  File? file;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    // Random random = Random();
    String? valueToClean = '21/90';
    //  valueToClean = (random.nextInt(899) + 100).toString();
    return Scaffold(
      backgroundColor: const Color(0xFFEBF0F3),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TweenAnimationBuilder(
              tween: Tween(begin: 0.0, end: 1.0),
              duration: const Duration(seconds: 3),
              builder: (context, value, child) {
                int percent = ((value as num) * 100).ceil();
                return Container(
                  alignment: Alignment.center,
                  width: width,
                  color: const Color(0xffebf0f3),
                  height: height * 0.43,
                  child: Column(
                    children: [
                      SizedBox(height: height * 0.0),
                      Stack(
                        children: <Widget>[
                          Positioned(
                            child: Image.asset(
                              "assets/images/first_circle.png",
                              width: 320,
                            ),
                          ),
                          Positioned(
                            top: 13.4,
                            left: 14,
                            child: SizeTransition(
                              sizeFactor: _animation2,
                              axisAlignment: -1,
                              axis: Axis.vertical,
                              child: Image.asset(
                                "assets/images/img_anim-modified.png",
                                width: 296.5,
                              ),
                            ),
                          ),
                          Positioned(
                            top: 12,
                            left: 12,
                            child: SizeTransition(
                              sizeFactor: _animation,
                              axisAlignment: -1,
                              axis: Axis.vertical,
                              child: Image.asset(
                                "assets/images/circle-512-modified.png",
                                width: 299,
                              ),
                            ),
                          ),
                          Positioned(
                            top: -17,
                            left: -17,
                            child: Image.asset(
                              "assets/images/second_circle.png",
                              width: 353,
                            ),
                          ),
                          Positioned(
                            top: 40,
                            left: 40,
                            child: Image.asset(
                              "assets/images/third_circle.png",
                              width: 260,
                            ),
                          ),
                          Positioned(
                            top: 141,
                            left: 115,
                            child: Text(
                              '$percent%',
                              //  valueToClean!,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                fontSize: 38,
                                fontWeight: FontWeight.w700,
                                color: Color.fromRGBO(7, 193, 251, 1),
                              ),
                            ),
                          ),
                          AnimatedPositioned(
                            duration: const Duration(milliseconds: 1500),
                            curve: Curves.linearToEaseOut,
                            top: selected ? 280 : 14,
                            left: 147,
                            child: AnimatedSwitcher(
                              duration: const Duration(milliseconds: 1500),
                              child: _renderWidget(),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
            Text(
              'System Scanning'.toUpperCase(),
              style: const TextStyle(
                color: Color.fromRGBO(62, 61, 73, 1),
                fontSize: 25,
                fontWeight: FontWeight.w400,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(235, 240, 243, 1),
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0xFFD1D9E6),
                      offset: Offset(4, 4),
                      blurRadius: 15,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: TweenAnimationBuilder(
                        tween: Tween(begin: 0.0, end: 2.0),
                        duration: const Duration(seconds: 3),
                        builder: (context, value, child) {
                          int percent = ((value as num) * 100).ceil();
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.only(
                                topRight: Radius.circular(15.0),
                                topLeft: Radius.circular(15.0),
                              ),
                              gradient: LinearGradient(
                                stops: [
                                  (value as double) * 0.1,
                                  (value as double) * 0.6,
                                  0.6,
                                ],
                                colors: const [
                                  Color.fromARGB(255, 34, 148, 242),
                                  Color.fromARGB(255, 209, 54, 244),
                                  Colors.white
                                ],
                              ),
                            ),
                            child: const SizedBox(height: 10.0),
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 15),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Image.asset(
                            'assets/images/carbon_security.png',
                            height: 28,
                            width: 28,
                          ),
                          const SizedBox(width: 18),
                          const Expanded(
                            child: Text(
                              'System Scanning',
                              style: TextStyle(
                                color: Color.fromRGBO(137, 166, 183, 1),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Text(
                            valueToClean,
                            style: const TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(235, 240, 243, 1),
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromARGB(20, 29, 1, 1),
                      offset: Offset(4, 4),
                      blurRadius: 20,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 1),
                      child: TweenAnimationBuilder(
                          tween: Tween(begin: 0.0, end: 2.0),
                          duration: const Duration(seconds: 3),
                          builder: (context, value, child) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.only(
                                  topRight: Radius.circular(15.0),
                                  topLeft: Radius.circular(15.0),
                                ),
                                gradient: LinearGradient(
                                  stops: [
                                    (value as double) * 0.1,
                                    (value as double) * 0.6,
                                    0.6,
                                  ],
                                  colors: const [
                                    Color.fromARGB(255, 34, 148, 242),
                                    Color.fromARGB(255, 209, 54, 244),
                                    Colors.white
                                  ],
                                ),
                              ),
                              child: const SizedBox(height: 10.0),
                            );
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 15),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/clarity_data-cluster-line.png',
                            height: 36,
                            width: 36,
                          ),
                          const SizedBox(width: 18),
                          const Expanded(
                            child: Text(
                              'System Task',
                              style: TextStyle(
                                color: Color.fromRGBO(137, 166, 183, 1),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          const Text(
                            '21/91',
                            style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(235, 240, 243, 1),
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromARGB(20, 29, 1, 1),
                      offset: Offset(4, 4),
                      blurRadius: 20,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(5),
                      decoration: const BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(15.0),
                          topLeft: Radius.circular(15.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 15),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/Vector.png',
                            height: 28,
                            width: 24,
                          ),
                          const SizedBox(width: 18),
                          const Expanded(
                            child: Text(
                              'System Failures',
                              style: TextStyle(
                                color: Color.fromRGBO(137, 166, 183, 1),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Image.asset(
                            'assets/images/Vector_color.png',
                            height: 28,
                            width: 24,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(235, 240, 243, 1),
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0xFFD1D9E6),
                      offset: Offset(-4, -4),
                      blurRadius: 15,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(5),
                      decoration: const BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(15.0),
                          topLeft: Radius.circular(15.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 15),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/ion_checkmark-done-circle-outline.png',
                            height: 29,
                            width: 29,
                          ),
                          const SizedBox(width: 18),
                          const Expanded(
                            child: Text(
                              'Apps security',
                              style: TextStyle(
                                color: Color.fromRGBO(137, 166, 183, 1),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Image.asset(
                            'assets/images/Vector_color.png',
                            height: 28,
                            width: 24,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
